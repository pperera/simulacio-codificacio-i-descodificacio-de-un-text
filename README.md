# Simulacio codificacio i descodificacio de un text

## Index:
- [Enunciat](#Enunciat)

- [Estrategia seguida](#Estrategia-seguida)

- [Procediment](#Procediment:)

- [Problemes del programa que encara no he resolt](#Problemes-del-programa-que-encara-no-he-resolt:)

- [Codi sense](#Codi-sense:)

- [Imatge amb el programa executat](#Imatge-amb-el-programa-executat:)

## Enunciat:

### Es demana fer un programa en C que permeti simular la compressio i la descompressio d'un text.

[☝️retorn a l'index](#index)
## Estrategia seguida:

### 1. Demanem l'entrada d'un text per teclat.

### 2. Després compactem el text seguint els següents passos:

#### 2.1 Mirem quins caràcters hi ha en aquest text, i quants cops apareixen al text.

#### 2.2 Codifiquem els caràcters, més repetits, amb els codis binaris més curts i els menys repetits amb els codis més llargs.

#### 2.3 Codifiquem el text d'acord als codis trobats a l'apartat 2.2 .

#### 2.4 Presentem un resum informatiu del resultat de la compressió. En aquest informe hem de reflectir:

##### 2.4.1 El nombre de bits del text.
##### 2.4.2 El nombre de bits del text comprimit i el % respecte el total.

##### 2.4.2 Una taula amb la relació entre caràcter, repetició i codi assignat.

### 3. Finalment demanem un altre cop el text per teclat, però, aquest cop el text compactat, i provem de descompactar-lo amb els mateixos codis trobats en la compactació, i així, obtenir el text original abans de compactar. Hem de treure per pantalla el resultat obtingut.
[☝️retorn a l'index](#index)
## Procediment:

### 1- Introduim les llibreries:

```js
#include <stdio.h>
#include <stdlib.h>
```

### 2- Declarem les constants per a tot el programa amb la estructura següent:

### #define + "identificado constant" + "valor"

### Nota: les constants han de complir els següents requisits:

#### 1. No poden enir el mateix nom que una “paraula reservada”.

#### 2. Només poden contenir lletres, dígits i guió baix. Però el primer caràcter no pot ser un número.

#### 3. No poden contenir espais.

#### 4. Tampoc pot tenir accents, ni dièresi, ni la lletra “ñ”…

### Introdueixo les següents constants:

```js
#define MAX_TEXT 100
#define MAX_TEXT2 200
#define MAX_REPETICIONS 100
#define MAX_CARACTERS 100
#define MAX_CODIS 43
#define MAX_DIGITS 5
#define BB while (getchar()!='\n')
```
### 3- Per indicar a on comença i acabara el programa escric:
### On "int main(){}" es per indicar tot el programa a executar.
### I el "return 0;" de dins el int main es per indicar on te de acabar de executar el programa.
```js
int main()
{    
    return 0;
}
```


### 4- Creo les variables següent especificant el tipus (char int o string) segons la informació a utilitzar a cada una de les variables.

### Estructura de les variables segóns caracters (char), numeros enters (int) o cadena de caracters, paraules (strings):

### Tipus enter (int):
#### · int + "nom variable" + ;

### Tipus caracer (char):
#### · char + "nom variable" + ;
#### I si vols definir el maxim de caracters de un text per exemple tens de declarar una constant amb el #define i el seu valor màxim.
#### Exemple: 
#### · char + "nom variable" + [constant del #define] + ;
### Tipus cadena o string:
#### · char + "nom variable" +  "[paraula] [caracter]" + ;

### Introdueixo les següents variables:

```js
    char text[MAX_TEXT+1],auxchar;
    char text3[MAX_TEXT2+1];
    char caracter[MAX_CARACTERS+1];
    int repeticions[MAX_REPETICIONS];
    int it,ic,i,i2,j,aux,bits1,bits2,bits3,comptador,comptador2;
    char codis[MAX_CODIS][MAX_DIGITS]={"0","1","00","01","10","11","000","001","010","100","011","110","101","111","0000","0001","0010","0100","1000","0011","0110","1100","1001","0101","1010","0111","1110","1101","1011","1111","00000","00001","00010","00100","01000","10000","00011","00110","01100","11000","10001","01010","10100"};

```

### 5- Faig les inicialitzacións
### Incialitzo amb \0  la llista i ho faig de aquesta manera:
```js
text[0]='\0';
```

### Incialitzo amb \0  els caracters i ho faig de aquesta manera:
```js
it=0;
    while(it<=MAX_CARACTERS){
        caracter[it]='\0';
        it++;
    }
```

### Incialitzo amb el valor "1"  les repeticions i ho faig de aquesta manera:
```js
it=0;
    while(it<MAX_REPETICIONS){
        repeticions[it]=1;
        it++;
    }
```
### 6- Imprimeixo per pantalla un text per avisar que ja pot introduir el text amb un printf.
```js
printf("Introdueix text: ");
```
### 7- I amb un scanf que guardi el text introduit a la variable "text"
```js
scanf("%100[^\n]",text);
```

### 8- Que fagi un recorregut pel vector de caracters sempre hi quan els caracters  del text i de la llista siguin diferents i el caracter de la llista no es un '\0'.
#### 8.1- Si tant el caracter esta a la llista o no que el compti (comptador) perque aixi més endavant podrem calcular el nombre de bits del text.
#### 8.2- Si el caracter no s'ha trobat a la llista l'afegeixi.
#### 8.2- Si el caracter existeix a la llista que no l'escrigui i el compti com una repetició.

```js
while(text[it]!='\0'){
        //recorregut pel vector de caracters sempre hi quan els caracters  del text i de la llista siguin diferents i el caracter de la llista no es un '\0'.
        ic=0;
        while(text[it]!=caracter[ic] &&
              caracter[ic]!='\0') ic++;
        //Si el caracter no s'ha trobat a la llista l'afegeix.
        if(caracter[ic]=='\0'){
            caracter[ic]=text[it];
            comptador++;
        //Si el caracter existeix a la llista que no l'escrigui i el compti com una repetició.
        }else{
            repeticions[ic]++;
            comptador++;
        }
        it++;
    }
```
### 9- Imprimeixo per pantalla el text introduït:
```js
printf("\nEl text es: %s",text);
```
### 10- Ara toca ordenar  el vector de repeticions juntament amb els dels caracters, i el codi seria el següen:
```js
ic=0;
    while(caracter[ic]!='\0') ic++;
    //MAX=ic
    for(i=2;i<=ic;i++){
        for(j=0;j<=ic-i;j++){
            if(repeticions[j]<repeticions[j+1]) {
                aux=repeticions[j];
                repeticions[j]=repeticions[j+1];
                repeticions[j+1]=aux;
                auxchar=caracter[j];
                caracter[j]=caracter[j+1];
                caracter[j+1]=auxchar;
            }
        }
    }
```

### 11- Ara imprimeixo per pantalla la llista de caracters, repeticions amb el codi assignat.
### El primer  caràcter, el d’índex 0, és el més repetit (estan ordenats descendentment). Per tant, el  primer codi, que és el més curt, s’assigna al primer caràcter que és el més repetit, i  així successivament.
```js
i=0;
    printf("\nCaracter \tRepeticions \tCodi");
    while(i<ic){
        printf("\n%c \t\t%d \t\t%s",caracter[i],repeticions[i],codis[i]);
        i++;
    }
```
### 12- Per fer la Codificació del text he fet el següent:
#### 12.1- Imprimeixo un text com a enunciat de a on sortira el text codificat.
#### 12.2-Imprimeix el text codificat.
### Amb el següent codi:
```js
printf("\nel text codificat es:");
    i=0;
    while (text[i]!='\0'){
        if (text[i]){printf("%s",codis[i]); printf("$");}
        i++;
    }
```
### 13- Per fer la descodificació del text he tingut de borrar bufer (BB;) avanç de fer el següent:
#### 13.1- He imprimit per pantalla un text com a enunciat per avisar que introdueix un text codificat.
#### 13.2- He fet un scanf per a un segon text (text3).
#### 13.3- He imprimit per pantalla un text com a enunciat de a on sortira el text descodificat. 
#### 13.4- He impres el text codificat
#### 13.5-  Fico un comptador (comptador2) per als caracters que no siguin un $ ni un \0 perque més endavant s'utilitzi a bits compactats.
### El codi es el següent:
```js
BB;
    i=0;
    comptador2=0;
    //Descodificació del text.
    printf("\nIntrodueix un text a descodificar:");
    scanf("%100[^\n]",text3);
    printf("\nEl text descodificat es:");
    while (text3[i]!='\0'){
        if (text3[i]){printf("%c",caracter[i]);}
        if (text3[i]!='$'){comptador2++;}
        i++;
    }

```
### 14- Finalment calculo els bits del text, els bites del text compactats, i el percentatge de compactació; d'aquesta manera:
```js
    bits1=comptador*8;
    bits2=comptador2;
    bits3=bits2*100/bits1;
    bits3=100-bits3;
    printf("\n Bits del text: %d", bits1);
    printf("\n Bits del text compactat: %d",bits2);
    printf("\n el precentatge de compactacio es: %d%%",bits3);
```

### 15- I per senyalitzar el final de la execució del programa acabem escribint al codi:
```js
        return 0;
}
```
[☝️retorn a l'index](#index)
## Problemes del programa que encara no he resolt:
### Al codificar i descodificar agafa els caracters de la llista en comptes del text i per aixo no surt el text ordenadament tal i com s'ha introduit.
[☝️retorn a l'index](#index)
## Codi sense:
```js
 #include <stdio.h>
#include <stdlib.h>

#define MAX_TEXT 100
#define MAX_TEXT2 200
#define MAX_REPETICIONS 100
#define MAX_CARACTERS 100
#define MAX_CODIS 43
#define MAX_DIGITS 5
#define BB while (getchar()!='\n')
int main()
{
    char text[MAX_TEXT+1],auxchar;
    char text3[MAX_TEXT2+1];
    char caracter[MAX_CARACTERS+1];
    int repeticions[MAX_REPETICIONS];
    int it,ic,i,i2,j,aux,bits1,bits2,bits3,comptador,comptador2;
    char codis[MAX_CODIS][MAX_DIGITS]={"0","1","00","01","10","11","000","001","010","100","011","110","101","111","0000","0001","0010","0100","1000","0011","0110","1100","1001","0101","1010","0111","1110","1101","1011","1111","00000","00001","00010","00100","01000","10000","00011","00110","01100","11000","10001","01010","10100"};


    //Inicialitzacions
    text[0]='\0';

    it=0;
    while(it<=MAX_CARACTERS){
        caracter[it]='\0';
        it++;
    }
    it=0;
    while(it<MAX_REPETICIONS){
        repeticions[it]=1;
        it++;
    }
    it=0;

    printf("Introdueix text: ");
    scanf("%100[^\n]",text);

    while(text[it]!='\0'){
        //recorregut pel vector de caracters sempre hi quan els caracters  del text i de la llista siguin diferents i el caracter de la llista no es un '\0'.
        ic=0;
        while(text[it]!=caracter[ic] &&
              caracter[ic]!='\0') ic++;
        //Si el caracter no s'ha trobat a la llista l'afegeix.
        if(caracter[ic]=='\0'){
            caracter[ic]=text[it];
            comptador++;
        //Si el caracter existeix a la llista que no l'escrigui i el compti com una repetició.
        }else{
            repeticions[ic]++;
            comptador++;
        }
        it++;
    }
    //sortida pantalla del text introduït
    printf("\nEl text es: %s",text);

    ic=0;
    while(caracter[ic]!='\0') ic++;

    //Ordenar vector de repeticions juntament amb els caracters
    ic=0;
    while(caracter[ic]!='\0') ic++;
    //MAX=ic
    for(i=2;i<=ic;i++){
        for(j=0;j<=ic-i;j++){
            if(repeticions[j]<repeticions[j+1]) {
                aux=repeticions[j];
                repeticions[j]=repeticions[j+1];
                repeticions[j+1]=aux;
                auxchar=caracter[j];
                caracter[j]=caracter[j+1];
                caracter[j+1]=auxchar;
            }
        }
    }
    i=0;
    printf("\nCaracter \tRepeticions \tCodi");
    while(i<ic){
        printf("\n%c \t\t%d \t\t%s",caracter[i],repeticions[i],codis[i]);
        i++;
    }
    //Codificació del text.
    printf("\nel text codificat es:");
    i=0;
    while (text[i]!='\0'){
        if (text[i]){printf("%s",codis[i]); printf("$");}
        i++;
    }

    BB;
    i=0;
    comptador2=0;
    //Descodificació del text.
    printf("\nIntrodueix un text a descodificar:");
    scanf("%100[^\n]",text3);
    printf("\nEl text descodificat es:");
    while (text3[i]!='\0'){
        if (text3[i]){printf("%c",caracter[i]);}
        if (text3[i]!='$'){comptador2++;}
        i++;
    }

    bits1=comptador*8;
    bits2=comptador2;
    bits3=bits2*100/bits1;
    bits3=100-bits3;
    printf("\n Bits del text: %d", bits1);
    printf("\n Bits del text compactat: %d",bits2);
    printf("\n el precentatge de compactacio es: %d%%",bits3);
    return 0;
}
```
[☝️retorn a l'index](#index)
## Imatge amb el programa executat:
![Git](captura.png)
[☝️retorn a l'index](#index)